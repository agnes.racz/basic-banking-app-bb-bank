package org.fasttrackit;


import java.util.ArrayList;
import java.util.List;

public class Customer {
    public static final String BLUE = "\u001B[34m";
    public static final String RESET = "\u001B[0m";
    public final String name;
    public final String address;
    public final int customerID;
    public final List<Account> accounts = new ArrayList<>();

    public Customer(String name, String address, int customerID) {
        this.name = name;
        this.address = address;
        this.customerID = customerID;
    }

    public char[] getAccountTypeOfCustomer() {
        char[] accType = new char[accounts.size()];
        for (int i = 0; i < accounts.size(); i++) {
            accType[i] = accounts.get(i).getAccountType();
        }
        return accType;
    }

    public int getAmountOfAnAccount(char accType) {
        int amount = 0;
        for (int i = 0; i < accounts.size(); i++) {
            if (accType == accounts.get(i).getAccountType()) {
                amount = accounts.get(i).getAmount();
                break;
            }
        }
        return amount;
    }

    public void setAmountInAccType(char accType, int newAmount) {
        for (int i = 0; i < accounts.size(); i++) {
            if (accType == accounts.get(i).getAccountType()) {
                accounts.get(i).setAmount(newAmount);
                break;
            }
        }
    }

    public String getAccountString() {
        String acc = "";

        if (accounts.isEmpty()) {
            acc = BLUE + "\nThis customer doesn't have open accounts." + RESET;
        } else {
            acc = "\n" + "Accounts:\n" + BLUE + "   ACCOUNT TYPE | INITIAL AMOUNT | CURRENT AMOUNT" + RESET;
            int i = 1;
            for (Account account : accounts) {
                acc += "\n" + i + ")\t\t" + account.getAccountType() + "\t\t|\t\t" + account.getInitialAmount() + "\t\t|\t\t" + account.getAmount();
                i++;
            }
        }
        return acc;
    }

    public int getCustomerID() {
        return customerID;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public void addAccount(char accountType, int initialAmount) {
        accounts.add(new Account(accountType, initialAmount));
    }
}
