package org.fasttrackit;

import java.util.ArrayList;
import java.util.List;

public class Manager {
    public static final String BLUE = "\u001B[34m";
    public static final String RESET = "\u001B[0m";
    private final String managerName;
    private final int managerID;
    private final String password;
    private final List<Customer> customers = new ArrayList<>();

    public Manager(String managerName, int managerID, String password) {
        this.managerName = managerName;
        this.managerID = managerID;
        this.password = password;
    }

    public char[] getAccountByCustomerIndex(int index) {
        return customers.get(index).getAccountTypeOfCustomer();
    }

    public int getAmountOfAnAccOfACustomer(int index, char accType) {
        return customers.get(index).getAmountOfAnAccount(accType);
    }

    public void setAmountOfAnAccOfACustomer(int index, char accType, int newAmount) {
        customers.get(index).setAmountInAccType(accType, newAmount);
    }


    public String getAccountsOfCustomer(int index) {
        return customers.get(index).getAccountString();
    }

    public int findCustomerIndex(int id) {
        int i;

        for (i = 0; i < customers.size(); i++) {
            if (id == customers.get(i).getCustomerID()) {
                break;
            }
        }
        return i;
    }

    public int getCustomerID(int i) {
        return customers.get(i).getCustomerID();
    }

    public int getTotalCustomers() {
        return customers.size();
    }

    public String getCustomer() {
        StringBuilder customerList = new StringBuilder("\nThe list with all your customers:\n " + BLUE + " NAME | ADDRESS | ID " + RESET + "\n");

        for (Customer customer : customers) {
            customerList.append(customer.getName()).append(" | ").append(customer.getAddress()).append(" | ").append(customer.getCustomerID()).append("\n");
        }
        return customerList.toString();
    }

    public String getManagerName() {
        return managerName;
    }

    public String getPassword() {
        return password;
    }

    public int getManagerID() {
        return managerID;
    }

    public void addAccountToCustomer(int customerIndex, char accountType, int initialAmount) {
        customers.get(customerIndex).addAccount(accountType, initialAmount);
    }

    public void addCustomer(String name, String address, int customerID) {
        customers.add(new Customer(name, address, customerID));
    }

}
