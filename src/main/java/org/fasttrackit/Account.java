package org.fasttrackit;

public class Account {
    private final char accountType;
    private final int initialAmount;
    private int amount;

    public Account(char accountType, int initialAmount) {
        this.accountType = accountType;
        this.initialAmount = initialAmount;
        this.amount = initialAmount;
    }

    public void setAmount(int newAmount) {
        amount = newAmount;
    }

    public int getAmount() {
        return amount;
    }

    public int getInitialAmount() {
        return initialAmount;
    }

    public char getAccountType() {
        return accountType;
    }
}
