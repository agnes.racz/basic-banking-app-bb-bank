package org.fasttrackit;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static final String GREEN = "\u001B[32m";
    public static final String RED = "\u001B[31m";
    public static final String BLUEBACK = "\u001B[44m";
    public static final String BLUE = "\u001B[34m";
    public static final String RESET = "\u001B[0m";
    public static final String NOCUSTOMER = "\n" + RED + "Customer was not found" + RESET;
    public static final String MENU = "\n\t\t" + BLUE + "MENU:" + RESET + "\n\t\t\t1) Add new customer\n\t\t\t2) Present a list with the registered customers\n\t\t\t3) Present a customer's accounts\n\t\t\t4) Add new account\n\t\t\t5) Deposit\n\t\t\t6) Withdraw\n\t\t\t7) Log out";
    private static final String INPUT_INVALID = RED + "Input Invalid!" + RESET;
    private static Scanner stringScanner;
    private static Scanner intScanner;
    public static List<Manager> managers = new ArrayList<>();
    public static final List<Integer> usedCustomerIDs = new ArrayList<>(); // a list with used customers ID, to exclude repeating an ID, when randomly geneating one.

    public static void main(String[] args) {

        registerManagers();

        int managerIndex = loginManager();
        int selection = 0;

        if (managerIndex != -1) {

            Manager loggedInManager = managers.get(managerIndex); // Saving the logged in Manager's details

            while (selection != 7) {
                System.out.println("\n\t\t" + loggedInManager.getManagerName() + ",\n\t\tWhat would you like to do today? ");
                selection = getSelectionFromMenu(); //input the number associated with the menu

                switch (selection) {
                    case 1: {
                        addNewCustomer(loggedInManager);
                        break;
                    }
                    case 2: {
                        System.out.println(loggedInManager.getCustomer());
                        break;
                    }
                    case 3: {
                        printCustomersAccounts(loggedInManager);
                        break;
                    }
                    case 4: {
                        addNewAccount(loggedInManager);
                        break;
                    }
                    case 5: {
                        deposit(loggedInManager);
                        break;
                    }
                    case 6: {
                        withdraw(loggedInManager);
                        break;

                    }
                    case 7:
                        System.out.println(GREEN + "Logged out successfully!" + RESET);
                        break;
                    default: {
                        break;
                    }
                }
            }
        }

    }

    private static void withdraw(Manager loggedInManager) {
        System.out.print("Please enter the customer ID:");
        int customerIDIN = intScanner.nextInt();
        boolean foundID = false;

        foundID = isFoundID(customerIDIN, loggedInManager, foundID);

        if (foundID) {
            for (int i = 0; i < loggedInManager.getTotalCustomers(); i++) {
                if (customerIDIN == loggedInManager.getCustomerID(i)) {

                    int index = loggedInManager.findCustomerIndex(customerIDIN);
                    char[] accountTypes = loggedInManager.getAccountByCustomerIndex(index);

                    showCustomerAccountTypes(accountTypes);

                    System.out.print("Please choose an account from above (S-Saving, C-Checking): ");
                    char accToModify = stringScanner.next().charAt(0);

                    int amountToModify = loggedInManager.getAmountOfAnAccOfACustomer(index, accToModify);

                    System.out.print("Please enter the amount to withdraw: ");
                    int withdrawAmount = intScanner.nextInt();
                    if (amountToModify >= withdrawAmount) {
                        int newAmount = amountToModify - withdrawAmount;

                        loggedInManager.setAmountOfAnAccOfACustomer(index, accToModify, newAmount);

                        System.out.println(GREEN + "Withdraw successful!" + RESET);
                        break;
                    } else System.out.println(RED + "Insufficient funds!!" + RESET);
                    break;
                }
            }
        } else {
            System.out.println(NOCUSTOMER);
        }
    }

    private static void deposit(Manager loggedInManager) {
        System.out.print("Please enter the customer ID:");
        int customerIDIN = intScanner.nextInt();
        boolean foundID = false;

        foundID = isFoundID(customerIDIN, loggedInManager, foundID); // searching for the customer associated with the logged in Manager

        if (foundID) {
            for (int i = 0; i < loggedInManager.getTotalCustomers(); i++) {
                if (customerIDIN == loggedInManager.getCustomerID(i)) {

                    int index = loggedInManager.findCustomerIndex(customerIDIN);
                    char[] accountTypes = loggedInManager.getAccountByCustomerIndex(index);

                    showCustomerAccountTypes(accountTypes);

                    System.out.print("Please choose an account from above (S-Saving, C-Checking): ");
                    char accToModify = stringScanner.next().charAt(0);

                    int amountToModify = loggedInManager.getAmountOfAnAccOfACustomer(index, accToModify);

                    System.out.print("Please enter the amount to deposit: ");
                    int addingAmount = intScanner.nextInt();

                    int newAmount = amountToModify + addingAmount;

                    loggedInManager.setAmountOfAnAccOfACustomer(index, accToModify, newAmount);

                    System.out.println(GREEN + "Deposit successful!" + RESET);
                    break;
                }
            }
        } else {
            System.out.println(NOCUSTOMER);
        }
    }

    private static void addNewAccount(Manager loggedInManager) {
        System.out.print("Please enter the customer ID:");
        int customerIDIN = intScanner.nextInt();
        boolean foundID = false;

        foundID = isFoundID(customerIDIN, loggedInManager, foundID);

        if (foundID) {
            int index = loggedInManager.findCustomerIndex(customerIDIN);
            char[] accountTypes = loggedInManager.getAccountByCustomerIndex(index);

            System.out.print("Please enter account type (S - Savings, C - Checking): ");
            char newAccountType = stringScanner.next().charAt(0);

            boolean hasAccountType = checkRegisteredAccountTypes(newAccountType, accountTypes);

            if (!hasAccountType) {
                System.out.print("Please enter initial amount: ");
                int initialAmount = intScanner.nextInt();

                loggedInManager.addAccountToCustomer(index, newAccountType, initialAmount);

                System.out.println("\n" + GREEN + "New account added successfully!" + RESET);
            }
        } else {
            System.out.println(NOCUSTOMER);
        }
    }


    private static void showCustomerAccountTypes(char[] accountTypes) {
        System.out.println("The customer accounts: ");
        for (int j = 0; j < accountTypes.length; j++) {
            System.out.println((j + 1) + ") " + accountTypes[j]);
        }
    }

    private static boolean checkRegisteredAccountTypes(char newAccountType, char[] accountTypes) { //Customer can only have 1 savings and 1 checking account. This function will check if a customer already has one
        boolean hasAccountType = false;

        switch (newAccountType) {
            case 'S' -> {
                for (char accountType : accountTypes) {
                    if (accountType == 'S') {
                        System.out.println("\n" + RED + "Customer already has a Savings account. Please go to 3) Present a customers accounts !" + RESET);
                        hasAccountType = true;
                        break;
                    }
                }
            }
            case 'C' -> {
                for (char accountType : accountTypes) {
                    if (accountType == 'C') {
                        System.out.println("\n" + RED + "Customer already has a Checking account. Please go to 3) Present a customers accounts" + RESET);
                        hasAccountType = true;
                        break;
                    }
                }
            }
            default -> System.out.println(INPUT_INVALID);
        }
        return hasAccountType;
    }

    private static boolean isFoundID(int customerIDIN, Manager loggedInManager, boolean foundID) {
        for (int i = 0; i < loggedInManager.getTotalCustomers(); i++) {
            if (customerIDIN == loggedInManager.getCustomerID(i)) {
                foundID = true;
                break;
            }
        }
        return foundID;
    }

    private static void printCustomersAccounts(Manager loggedInManager) {
        System.out.print("Please enter the customer ID:");
        int customerIDIN = intScanner.nextInt();
        boolean foundID = false;
        int totalCustomers = loggedInManager.getTotalCustomers(); //saving the number of customers the logged in Manager has

        for (int i = 0; i < totalCustomers; i++) {
            if (customerIDIN == loggedInManager.getCustomerID(i)) { // if the customer was found
                int index = loggedInManager.findCustomerIndex(customerIDIN); // find the customer's index

                System.out.println(loggedInManager.getAccountsOfCustomer(index)); // print the accounts of the customer
                foundID = true;
                break; // if the customer was found, finish the investigation
            }
        }
        if (!foundID)
            System.out.println(NOCUSTOMER);
    }

    private static int generateCustomerID() {
        int min = 10000;
        int max = 99999;
        int alreadyUsed = 0;
        int randomID = 0;

        do { //generate a random id, and check if it was already used or no
            randomID = (int) Math.floor(Math.random() * (max - min + 1) + min);
            for (Integer usedCustomerID : usedCustomerIDs) {
                if (randomID == usedCustomerID) {
                    alreadyUsed = 1;
                    break;
                }
            }
        } while (alreadyUsed != 0);

        usedCustomerIDs.add(randomID); // adding the generated ID to the list, so it will not be used for another customer

        return randomID;
    }

    private static String addCustomerAddress() {
        System.out.print("Address: ");
        String address = stringScanner.nextLine();
        while (!hasNoSpecChar(address)) {
            System.out.print(RED + "Address format incorrect, must not contain special characters. Please try again: " + RESET);
            address = stringScanner.nextLine();
        }
        address = address.toUpperCase();
        return address;
    }

    private static boolean hasNoSpecChar(String name) {
        int i;

        for (i = 0; i < name.length(); i++) {
            if (!Character.isDigit(name.charAt(i))
                    && !Character.isLetter(name.charAt(i))
                    && !Character.isWhitespace(name.charAt(i))) {
                break; // if spec char was found, break from investigation
            }
        }
        return i == name.length(); //returning true if "for" was fully done
    }

    private static boolean hasNoDigit(String name) {
        int i;

        for (i = 0; i < name.length(); i++) {
            if (Character.isDigit(name.charAt(i))) {
                break; //if digit was found, break from investigation
            }
        }
        return i == name.length(); //returning true if "for" was fully done
    }

    private static String addCustomerName() {
        System.out.print("Name: ");
        String name = stringScanner.nextLine();

        while (!hasNoDigit(name) || !hasNoSpecChar(name)) {  // checking if the input name has any digits or spec char
            System.out.print(RED + "Name format incorrect, must not contain numbers or special characters. Please try again: " + RESET);
            name = stringScanner.nextLine();
        }
        name = name.toUpperCase();
        return name;
    }

    private static void addNewCustomer(Manager loggedInManager) {
        String name = addCustomerName();
        String address = addCustomerAddress();
        int customerID = generateCustomerID();

        loggedInManager.addCustomer(name, address, customerID);

        System.out.println(GREEN + "Customer added successfully! Customer Id is: " + RESET + customerID);
    }

    private static int getSelectionFromMenu() {
        int selection;

        System.out.println(MENU);
        System.out.print("Please enter a number: ");
        selection = intScanner.nextInt();

        return selection;
    }

    private static int loginManager() {
        System.out.print("\n\t\t" + BLUEBACK + "Welcome to BB Bank!" + RESET + " \n(*MANAGER ID: 1111 / 2222 / 3333 PASSWORD: john1 / sharon2 / peter3)\n\n\t\tPlease login:\n\t\t\tMANAGER ID:");
        stringScanner = new Scanner(System.in);
        intScanner = new Scanner(System.in);
        int manID = intScanner.nextInt(); // input the manager ID
        int managerIndex = -1;
        String pass = "";

        for (int i = 0; i < managers.size(); i++) { //find the password associated with the input Manager ID
            if (manID == managers.get(i).getManagerID()) {
                pass = managers.get(i).getPassword();
                managerIndex = i;
                break;
            }
        }
        if (pass.isEmpty()) { //if couldn't find a password, the Manager doesn't exist
            System.out.println(INPUT_INVALID);
        } else {
            System.out.print("\t\t\tPASSWORD:"); //if password is found, ask for password input and compare with the previously found password
            String passIn = stringScanner.nextLine();

            if (!pass.equals(passIn)) {
                System.out.println(INPUT_INVALID);
                managerIndex = -1;
            } else {
                System.out.println("\t\t" + GREEN + "Login successful!" + RESET); //if password is correct, Manager is logged in
            }
        }
        return managerIndex; // returning -1 if input was invalid or the value of i - to save the Manager Index
    }

    private static void registerAccounts(Manager john, Manager sharon, Manager peter) {
        john.addAccountToCustomer(0, 'S', 100);
        john.addAccountToCustomer(0, 'C', 200);
        john.addAccountToCustomer(1, 'C', 0);
        john.addAccountToCustomer(2, 'S', 4000);

        sharon.addAccountToCustomer(1, 'S', 0);
        sharon.addAccountToCustomer(0, 'S', 0);

        peter.addAccountToCustomer(0, 'S', 0);
    }

    private static void registerCustomers(Manager john, Manager sharon, Manager peter) {
        john.addCustomer("REBECCA", "CLUJ 14", 12345);
        john.addCustomer("GERTRUD", "CLUJ 45", 23456);
        john.addCustomer("PAULA", "TURDA 48", 34567);

        sharon.addCustomer("WILL", "BUCHAREST 14", 45678);
        sharon.addCustomer("ENID", "NEW YORK 78", 56789);

        peter.addCustomer("ROBIN", "TURDA 7", 98765);
        peter.addCustomer("TAYLOR", "PARIS 123", 87654);

        usedCustomerIDs.add(12345);
        usedCustomerIDs.add(23456);
        usedCustomerIDs.add(34567);
        usedCustomerIDs.add(45678);
        usedCustomerIDs.add(56789);
        usedCustomerIDs.add(98765);
        usedCustomerIDs.add(87654);

        registerAccounts(john, sharon, peter);
    }

    private static void registerManagers() {
        Manager john = new Manager("John Smith", 1111, "john1");
        Manager sharon = new Manager("Sharon Queen", 2222, "sharon2");
        Manager peter = new Manager("Peter Flag", 3333, "peter3");

        registerCustomers(john, sharon, peter);

        managers.add(john);
        managers.add(sharon);
        managers.add(peter);
    }
}
